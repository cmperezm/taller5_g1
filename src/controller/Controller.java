package controller;

import java.util.Scanner;

import model.logic.MVCModelo;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
				case 1:
				    modelo.cargar();
									
					break;

				case 2:
					System.out.println("ingrese el trimestre ");
					int trimestre= lector.nextInt();
					System.out.println("ingrese el sourceId ");
					int s= lector.nextInt();
					System.out.println("ingrese el dsId ");
					int d= lector.nextInt();
					modelo.poblarTablaLinearProbing(trimestre, s, d);
					break;

				case 3:
					System.out.println("ingrese el trimestre ");
					int t= lector.nextInt();
					System.out.println("ingrese el sourceId ");
					int so= lector.nextInt();
					System.out.println("ingrese el dsId ");
					int ds= lector.nextInt();
					modelo.poblarTablaSeparateCahining(t, so, ds);
					break;

				case 4:
					System.out.println("ingrese el trimestre ");
					int tri= lector.nextInt();
					System.out.println("ingrese el sourceId ");
					int sou= lector.nextInt();
					System.out.println("ingrese el dsId ");
					int dsi= lector.nextInt();
					modelo.BuscarLinear(tri, sou, dsi);
					break;

				case 5: 
					System.out.println("ingrese el trimestre ");
					int trim= lector.nextInt();
					System.out.println("ingrese el sourceId ");
					int sour= lector.nextInt();
					System.out.println("ingrese el dsId ");
					int dsid= lector.nextInt();
					modelo.BuscarSeparate(trim, sour, dsid);break;	
					
				case 6: 
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					break;	

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
