package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import model.data_structures.LinearProbingHashST;
import model.data_structures.Queue;
import model.data_structures.SeparateChainingHashST;


public class MVCModelo {
	/**
	 * Atributos del modelo del mundo
	 */
	 private Queue<UberTrip> queue1=new Queue<UberTrip>();
	 private Queue<UberTrip> queue2=new Queue<UberTrip>();
	 private Queue<UberTrip> queue3=new Queue<UberTrip>();
	 private Queue<UberTrip> queue4=new Queue<UberTrip>();
	 private LinearProbingHashST<String, Double> hashlinear = new LinearProbingHashST<>();
	 private SeparateChainingHashST<String, Double> hashseparate = new SeparateChainingHashST<>();
	public void cargar() {


		for (int i = 1; i <= 4; i++) {
			String line = "";
			File fileWeekly = new File("./data/bogota-cadastral-2018-" + i + "-WeeklyAggregate.csv");
			try (BufferedReader br2 = new BufferedReader(new FileReader(fileWeekly))) {
				br2.readLine();
				line = br2.readLine();

				while (line != null) {
					String[] data = line.split(",");
					int dato1 = Integer.parseInt(data[0]);
					int dato2 = Integer.parseInt(data[1]);
					int dato3 = Integer.parseInt(data[2]);
					double dato4 = Double.parseDouble(data[3]);
					double dato5 = Double.parseDouble(data[4]);

					UberTrip viaje = new UberTrip(dato1,dato2,dato3,dato4,dato5);
					if(i==1){
						queue1.enqueue(viaje);
						System.out.println("Primer viaje, zona origen: "+viaje.getSourceID()+", zona destino: "+viaje.getdsId()+", dia: "+viaje.getTime()+", tiempo promedio: "+viaje.getMeanTravelTime() );
					}
					else if(i==2)
						queue2.enqueue(viaje);
					else if(i==3)
						queue1.enqueue(viaje);
					else if(i==4)
						queue3.enqueue(viaje);

					line = br2.readLine();

				}
				br2.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Primer viaje, zona origen: "+queue1.peek().getSourceID()+", zona destino: "+queue1.peek().getdsId()+", dia: "+queue1.peek().getTime()+", tiempo promedio: "+queue1.peek().getMeanTravelTime() );
			System.out.println("Ultimo viaje, zona origen: "+queue1.getLast().getSourceID()+", zona destino: "+queue1.getLast().getdsId()+", dia: "+queue1.getLast().getTime()+", tiempo promedio: "+queue1.getLast().getMeanTravelTime());
			System.out.println("Numero de viajes:"+queue1.size());

			System.out.println("Primer viaje, zona origen: "+queue2.peek().getSourceID()+", zona destino: "+queue2.peek().getdsId()+", dia: "+queue2.peek().getTime()+", tiempo promedio: "+queue2.peek().getMeanTravelTime() );
			System.out.println("Ultimo viaje, zona origen: "+queue2.getLast().getSourceID()+", zona destino: "+queue2.getLast().getdsId()+", dia: "+queue2.getLast().getTime()+", tiempo promedio: "+queue2.getLast().getMeanTravelTime());
			System.out.println("Numero de viajes:"+queue2.size());

			System.out.println("Primer viaje, zona origen: "+queue3.peek().getSourceID()+", zona destino: "+queue3.peek().getdsId()+", dia: "+queue3.peek().getTime()+", tiempo promedio: "+queue3.peek().getMeanTravelTime() );
			System.out.println("Ultimo viaje, zona origen: "+queue3.getLast().getSourceID()+", zona destino: "+queue3.getLast().getdsId()+", dia: "+queue3.getLast().getTime()+", tiempo promedio: "+queue3.getLast().getMeanTravelTime());
			System.out.println("Numero de viajes:"+queue3.size());

			System.out.println("Primer viaje, zona origen: "+queue4.peek().getSourceID()+", zona destino: "+queue4.peek().getdsId()+", dia: "+queue4.peek().getTime()+", tiempo promedio: "+queue4.peek().getMeanTravelTime() );
			System.out.println("Ultimo viaje, zona origen: "+queue4.getLast().getSourceID()+", zona destino: "+queue4.getLast().getdsId()+", dia: "+queue4.getLast().getTime()+", tiempo promedio: "+queue4.getLast().getMeanTravelTime());
			System.out.println("Numero de viajes:"+queue4.size());
		}
	}
	public void poblarTablaLinearProbing(int trimestre,int sourceid,int dsid)
	{
		
		Iterator<UberTrip> it=queue1.iterator();
		if(trimestre==2)
		it=queue2.iterator();
		else if(trimestre==3)
		{
			it=queue3.iterator();
		}
		else if(trimestre==4)
		{
			it=queue4.iterator();
		}
		while(it.hasNext())
		{
			UberTrip actual = (UberTrip) it;
			if(actual.getSourceID()==sourceid && actual.getdsId()==dsid)
			{
				String key =""+trimestre+"-"+sourceid+"-"+dsid;
				hashlinear.put(key, actual.getMeanTravelTime());
			}
		}
	}
	
	public void poblarTablaSeparateCahining(int trimestre,int sourceid,int dsid)
	{
		
		Iterator<UberTrip> it=queue1.iterator();
		if(trimestre==2)
		it=queue2.iterator();
		else if(trimestre==3)
		{
			it=queue3.iterator();
		}
		else if(trimestre==4)
		{
			it=queue4.iterator();
		}
		while(it.hasNext())
		{
			UberTrip actual = (UberTrip) it;
			if(actual.getSourceID()==sourceid && actual.getdsId()==dsid)
			{
				String key =""+trimestre+"-"+sourceid+"-"+dsid;
				hashseparate.put(key, actual.getMeanTravelTime());
			}
		}
	}
	public void BuscarLinear(int trimestre,int sourceID,int dsId)
	{
		Iterator<String> it=hashlinear.keys().iterator();
		
		while(it.hasNext())
		{
			String key =""+trimestre+"-"+sourceID+"-"+dsId;
			double a = hashlinear.get(key);
		}
	}
	
	public void BuscarSeparate(int trimestre,int sourceID,int dsId)
	{
		
	}
}	
