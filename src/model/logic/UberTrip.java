package model.logic;

public class UberTrip {

	//---------
	//Atributos
	//---------

	/**
	 * ID zona de origen, ID zona destino y tiempo
	 */
	private int sourceID,dsId,time;

	/**
	 * tiempo promedio, desviacion estandar y tiempo promedio geometrico, desviacion estandar geometrico
	 */
	private double mean_Travel_Time,standard_Deviation_travel_Time;

	//---------
	//Atributos
	//---------

	/**
	 * constructor de un viaje
	 * @param p1 ID zona origen
	 * @param p2 ID zona destino
	 * @param p3 tiempo
	 * @param p4 tiempo promedio de viaje
	 * @param p5 desviacion estandar
	 */
	public UberTrip(int p1,int p2,int p3,double p4,double p5){
		sourceID=p1;
		dsId=p2;
		time=p3;
		mean_Travel_Time = p4;
		standard_Deviation_travel_Time=p5;


	}

	/**
	 * retorna el ID de zona de inicio
	 * @return sourceID
	 */
	public int getSourceID()
	{
		return sourceID;
	}

	/**
	 * retorna el ID de zona de fin
	 * @return dsID
	 */
	public int getdsId()
	{
		return dsId;
	}

	/**
	 * retorna el tiempo
	 * @return time
	 */
	public int getTime()
	{
		return time;
	}

	/**
	 * retonra el tiempo promedio de viaje
	 * @return meantraveltime
	 */
	public double getMeanTravelTime()
	{
		return mean_Travel_Time;
	}

	/**
	 * retonra la desviacion estandar
	 * @return standar_deviation
	 */
	public double getStandardDeviation()
	{
		return standard_Deviation_travel_Time;
	}


	/**
	 * comparacion entre viajes 
	 * @param other viaje a comparar
	 * @return 1 si el actual es mayor, -1 si el actual es menor, 0 si es igual
	 */
	public int compareTo(UberTrip other) {
		// TODO Auto-generated method stub

		if(this.mean_Travel_Time > other.mean_Travel_Time){
			return 1;
		} else if(this.mean_Travel_Time < other.mean_Travel_Time){
			return -1;
		} else if(this.standard_Deviation_travel_Time > other.standard_Deviation_travel_Time){
			return 1;
		} else if(this.standard_Deviation_travel_Time < other.standard_Deviation_travel_Time){
			return -1;
		}
		return 0;

	}
}
